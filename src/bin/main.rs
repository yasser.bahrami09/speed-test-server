extern crate speed_test_server;

use std::{env, fs, io, thread};
use std::io::{Read, Write};
use std::net::{TcpListener, Shutdown};
use std::net::TcpStream;
use std::str::FromStr;
use std::time::{Duration, Instant};

use chrono::prelude::*;
use rand::random;
use serde::{Deserialize, Serialize};

use speed_test_server::ThreadPool;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() <= 1 {
        panic!("config file missing!");
    }
    let config_str: String = fs::read_to_string(&args[1]).unwrap();
    let config: Config = serde_json::from_str(&config_str).unwrap();
    let protocol: Protocol = PerformanceMetrics::new(ProtocolType::from_str(config.protocol.as_ref()).unwrap());
    protocol.speed_test(config.ip, config.port, config.timeout, config.upload_time, config.download_time);
}

fn handle_connection(mut stream: TcpStream, timeout: u64, upload_time: u64, download_time: u64) {
    let mut buffer = [0; 8192];

    let upload_finish_command = b"upload test finish";
    let download_finish_command = b"download test finish";
    let read_timeout: Option<Duration> = Option::from(Duration::from_secs(timeout));
    let write_timeout: Option<Duration> = Option::from(Duration::from_secs(timeout));
    let _read_return: io::Result<()> = stream.set_read_timeout(read_timeout);
    let _write_return: io::Result<()> = stream.set_write_timeout(write_timeout);
    let download_duration = Duration::from_secs(download_time + 5);
    let mut start = Instant::now();
    loop {
        let _read_res = stream.read(&mut buffer);
        if buffer.starts_with(upload_finish_command) {
            break;
        }
        if start.elapsed() >= download_duration {
            break;
        }
        thread::sleep(Duration::from_millis(1));
    }
    let random_bytes: Vec<u8> = (0..8192).map(|_| { random::<u8>() }).collect();
    start = Instant::now();
    let upload_duration = Duration::from_secs(upload_time);
    loop {
        let _write_res = stream.write(random_bytes.as_slice());
        match _write_res {
            Ok(_n) => {},
            Err(e) => {println!("error write {:?}", e)}
        }
        let _flush_res = stream.flush();
        if start.elapsed() >= upload_duration {
            break;
        }
        thread::sleep(Duration::from_millis(1));
    }
    let _write_res = stream.write(download_finish_command);
    let _flush_res = stream.flush();
    println!("speed test finished for {}", stream.peer_addr().unwrap());
    let _stream = stream.shutdown(Shutdown::Both);
}

#[derive(Clone, Debug, Serialize, Deserialize)]
struct Config {
    ip: String,
    port: i32,
    protocol: String,
    timeout: u64,
    download_time: u64,
    upload_time: u64,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum ProtocolType {
    Tcp,
    Udp,
    Rds,
}

impl FromStr for ProtocolType {
    type Err = ();

    fn from_str(input: &str) -> Result<ProtocolType, Self::Err> {
        match input {
            "tcp" => Ok(ProtocolType::Tcp),
            "udp" => Ok(ProtocolType::Udp),
            "rds" => Ok(ProtocolType::Rds),
            _ => Err(()),
        }
    }
}

struct Protocol {
    protocol_type: ProtocolType
}

trait PerformanceMetrics {
    fn new(protocol_type: ProtocolType) -> Self;
    fn speed_test(&self, ip: String, port: i32, timeout: u64, upload_time: u64, download_time: u64) -> bool;
}

impl PerformanceMetrics for Protocol {
    fn new(protocol_type: ProtocolType) -> Protocol {
        Protocol { protocol_type: protocol_type }
    }
    fn speed_test(&self, ip: String, port: i32, timeout: u64, upload_time: u64, download_time: u64) -> bool {
        if timeout <= 0 {
            println!("invalid timeout value {}", timeout);
            return false;
        } else if port > 65535 || port < 0 {
            println!("invalid port value {}", port);
            return false;
        } else if upload_time <= 0 {
            println!("invalid upload time value {}", upload_time);
            return false;
        } else if download_time <= 0 {
            println!("invalid download time value {}", download_time);
            return false;
        }
        return match self.protocol_type {
            ProtocolType::Tcp => {
                let listener = TcpListener::bind(format!("{}:{}", ip, port)).unwrap();
                let pool = ThreadPool::new(10);
                for stream in listener.incoming() {
                    let stream = stream.unwrap();
                    let now: DateTime<Utc> = Utc::now();
                    println!("[{}] {}", now.format("%Y-%m-%d %H:%M:%S"), stream.peer_addr().unwrap());
                    pool.execute(move || {
                        handle_connection(stream, timeout, upload_time, download_time);
                    });
                    /*let _result = thread::Builder::new().spawn(move || {
                        handle_connection(stream, timeout, upload_time, download_time);
                    });*/
                }
                true
            }
            _ => {
                println!("speed test for this protocol is not implemented");
                false
            }
        };
    }
}

#[cfg(test)]
mod tests {
    use std::panic;

    use super::*;

    #[test]
    fn test_speed_test_invalid_timeout() {
        let protocol: Protocol = PerformanceMetrics::new(ProtocolType::Tcp);
        assert_eq!(protocol.speed_test("127.0.0.1".to_string(), 8080, 0, 30000, 30000), false);
    }

    #[test]
    fn test_speed_test_invalid_port() {
        let protocol: Protocol = PerformanceMetrics::new(ProtocolType::Tcp);
        assert_eq!(protocol.speed_test("127.0.0.1".to_string(), 80800, 1000, 30000, 30000), false);
    }

    #[test]
    fn test_speed_test_invalid_upload_time() {
        let protocol: Protocol = PerformanceMetrics::new(ProtocolType::Tcp);
        assert_eq!(protocol.speed_test("127.0.0.1".to_string(), 8080, 1000, 0, 30000), false);
    }

    #[test]
    fn test_speed_test_invalid_download_time() {
        let protocol: Protocol = PerformanceMetrics::new(ProtocolType::Tcp);
        assert_eq!(protocol.speed_test("127.0.0.1".to_string(), 8080, 1000, 30000, 0), false);
    }

    #[test]
    fn test_speed_test_udp() {
        let protocol: Protocol = PerformanceMetrics::new(ProtocolType::Udp);
        assert_eq!(protocol.speed_test("127.0.0.1".to_string(), 8080, 1000, 30000, 30000), false);
    }

    #[test]
    fn test_speed_test_invalid_address() {
        let protocol: Protocol = PerformanceMetrics::new(ProtocolType::Tcp);
        let result = panic::catch_unwind(|| {
            protocol.speed_test("10.11.12.13".to_string(), 8080, 1000, 30000, 30000);
        });
        assert!(result.is_err());
    }
}